package multimidia.mmfit2;



public class Exercicio {
    private int codigo;
    private String nome;
    private int imagemDrawable;
    private int imagemMaiorDrawable;

    public Exercicio(int codigo, String nome){
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getImagemDrawable() {
        return imagemDrawable;
    }

    public void setImagemDrawable(int imagemDrawable) {
        this.imagemDrawable = imagemDrawable;
    }

    public int getImagemMaiorDrawable() {
        return imagemMaiorDrawable;
    }

    public void setImagemMaiorDrawable(int imagemMaiorDrawable) {
        this.imagemMaiorDrawable = imagemMaiorDrawable;
    }

}
