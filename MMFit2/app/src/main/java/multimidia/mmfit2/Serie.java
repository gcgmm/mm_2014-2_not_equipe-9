package multimidia.mmfit2;


import java.util.ArrayList;

public class Serie {
    private int codigoAluno;
    //private int codigo;
    private ArrayList<Atividade> atividades;
    private String comentario;

    public Serie(/*int codigo, */int codigoAluno){
        //this.codigo = codigo;
        this.codigoAluno = codigoAluno;
        atividades = new ArrayList<Atividade>();
    }

    public int getCodigoAluno() {
        return codigoAluno;
    }

    public void setCodigoAluno(int codigoAluno) {
        this.codigoAluno = codigoAluno;
    }

    /*public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }*/

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public ArrayList<Atividade> getAtividades() {
        return atividades;
    }

    public void addAtividade(int codigoExercicio, long tempoPrevisto, long tempo, double quantidadePrevista){
        atividades.add(new Atividade(atividades.size(), codigoExercicio, tempoPrevisto, tempo, quantidadePrevista));
    }


}
