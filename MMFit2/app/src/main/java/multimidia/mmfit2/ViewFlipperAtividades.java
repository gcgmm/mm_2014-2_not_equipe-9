package multimidia.mmfit2;


import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import java.util.concurrent.TimeUnit;

public class ViewFlipperAtividades extends ViewFlipper {

    private Controle controle;
    private int idsTextView[][];
    private int posicaoAtual;
    private float posicaoToque;
    private TextView tvTempo;
    private TextView tvQuantidade;
    private TextView tvExtra;

    public ViewFlipperAtividades(final Controle controle, Context context) {
        super(context);
        this.controle = controle;

        layout(context);

        setOnTouchListener(new android.view.View.OnTouchListener() {

            @Override
            public boolean onTouch(android.view.View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    posicaoToque = event.getX();
                } else if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(posicaoToque + 200 < event.getX() && posicaoAtual > 0){
                        setInAnimation(inFromLeftAnimation());
                        setOutAnimation(outToRightAnimation());

                        showPrevious();
                        posicaoAtual--;
                        controle.mudarAtividade(posicaoAtual);

                    }else if(posicaoToque - 200 > event.getX() && posicaoAtual < controle.getSerieEmExecucao().getAtividades().size()-1){
                        setInAnimation(inFromRightAnimation());
                        setOutAnimation(outToLeftAnimation());

                        showNext();
                        posicaoAtual++;
                        controle.mudarAtividade(posicaoAtual);
                    }
                    tvTempo = (TextView)findViewById(idsTextView[posicaoAtual][0]);
                    tvQuantidade = (TextView)findViewById(idsTextView[posicaoAtual][1]);
                    tvExtra = (TextView)findViewById(idsTextView[posicaoAtual][2]);
                }
                return true;
            }
        });
    }

    private void layout(Context context){
        posicaoAtual = 0;

        Serie serie = controle.getSerieEmExecucao();
        idsTextView = new int[serie.getAtividades().size()][3];

        for(Atividade ativ:serie.getAtividades()) {
            LinearLayout ll = new LinearLayout(context);
            ll.setOrientation(LinearLayout.VERTICAL);
            if(posicaoAtual % 2 == 0) {
                ll.setBackgroundColor(Color.CYAN);
            }else{
                ll.setBackgroundColor(Color.LTGRAY);
            }

            TextView numero = new TextView(context);
            numero.setText("Exercício " + (posicaoAtual + 1) + " de " + serie.getAtividades().size());
            numero.setTextSize(30);
            numero.setGravity(Gravity.CENTER_HORIZONTAL);
            ll.addView(numero);

            TextView tempo = new TextView(context);
            tempo.setText(String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(ativ.getTempo()),
                    TimeUnit.MILLISECONDS.toMinutes(ativ.getTempo()) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ativ.getTempo())),
                    TimeUnit.MILLISECONDS.toSeconds(ativ.getTempo()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((ativ.getTempo())))));
            tempo.setTextSize(50);
            idsTextView[posicaoAtual][0] = generateViewId();
            tempo.setId(idsTextView[posicaoAtual][0]);
            tempo.setGravity(Gravity.CENTER_HORIZONTAL);

            ll.addView(tempo);

            TextView descricao = new TextView(context);
            descricao.setText(controle.getExercicio(ativ.getCodigoExercicio()).getNome());
            descricao.setTextSize(25);
            descricao.setGravity(Gravity.CENTER_HORIZONTAL);
            ll.addView(descricao);

            ImageView imagem = new ImageView(controle.getView());
            imagem.setImageResource(controle.getExercicio(ativ.getCodigoExercicio()).getImagemMaiorDrawable());
            ll.addView(imagem);

            if(controle.getExercicio(ativ.getCodigoExercicio()).getClass().equals(Corrida.class)) {
                TextView quantidade = new TextView(context);
                if((int) (ativ.getQuantidadePrevista() - ativ.getQuantidade()) > 0) {
                    quantidade.setText("Metros: " + (int) (ativ.getQuantidadePrevista() - ativ.getQuantidade()));
                }else {
                    quantidade.setText("Terminou");
                }
                quantidade.setTextSize(50);
                quantidade.setGravity(Gravity.CENTER_HORIZONTAL);
                idsTextView[posicaoAtual][1] = generateViewId();
                quantidade.setId(idsTextView[posicaoAtual][1]);
                ll.addView(quantidade);
            }


            if(controle.getExercicio(ativ.getCodigoExercicio()).getClass().equals(Movimento.class)) {
                TextView extra = new TextView(context);
                extra.setTextSize(50);
                extra.setGravity(Gravity.CENTER_HORIZONTAL);
                idsTextView[posicaoAtual][2] = generateViewId();
                extra.setId(idsTextView[posicaoAtual][2]);
                ll.addView(extra);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
            ll.setLayoutParams(params);
            addView(ll);

            posicaoAtual++;
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        setLayoutParams(params);

        posicaoAtual = 0;
        tvTempo = (TextView)findViewById(idsTextView[0][0]);
        tvQuantidade = (TextView)findViewById(idsTextView[0][1]);
        tvExtra = (TextView)findViewById(idsTextView[0][2]);
    }

    public void setTempo(String tempo){
        tvTempo.setText(tempo);
    }

    public void setQuantidade(String quantidade){
        tvQuantidade.setText(quantidade);
    }

    public void setExtra(String extra) {
        tvExtra.setText(extra);
    }

    private Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT,  +1.0f, Animation.RELATIVE_TO_PARENT,  0.0f,
                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromRight.setDuration(250);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }
    private Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,  -1.0f,
                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
        );
        outtoLeft.setDuration(250);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    private Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT,  -1.0f, Animation.RELATIVE_TO_PARENT,  0.0f,
                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
        );
        inFromLeft.setDuration(250);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }
    private Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,  +1.0f,
                Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
        );
        outtoRight.setDuration(250);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }


}
