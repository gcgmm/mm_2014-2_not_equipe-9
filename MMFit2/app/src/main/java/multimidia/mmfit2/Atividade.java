package multimidia.mmfit2;



public class Atividade {
    private int codigo;
    private int codigoExercicio;
    private long tempo, tempoPrevisto;
    private double quantidade, quantidadePrevista;

    public Atividade(int codigo, int codigoExercicio, long tempoPrevisto, long tempo, double quantidadePrevista) {
        this.codigo = codigo;
        this.codigoExercicio = codigoExercicio;
        this.tempoPrevisto = tempoPrevisto;
        this.quantidadePrevista = quantidadePrevista;
        this.tempo = tempo;
    }

    public long getTempo() {
        return tempo;
    }

    public void setTempo(long tempo) {
        this.tempo = tempo;
    }

    public long getTempoPrevisto() {
        return tempoPrevisto;
    }

    public void setTempoPrevisto(long tempoPrevisto) {
        this.tempoPrevisto = tempoPrevisto;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getQuantidadePrevista() {
        return quantidadePrevista;
    }

    public void setQuantidadePrevista(double quantidadePrevista) {
        this.quantidadePrevista = quantidadePrevista;
    }

    public int getCodigoExercicio() {
        return codigoExercicio;
    }

    public void setCodigoExercicio(int codigoExercicio) {
        this.codigoExercicio = codigoExercicio;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
