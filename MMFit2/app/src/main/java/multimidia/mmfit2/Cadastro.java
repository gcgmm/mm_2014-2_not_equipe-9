package multimidia.mmfit2;


import java.util.ArrayList;

public class Cadastro {
    private ArrayList<Aluno> alunos;
    private ArrayList<Exercicio> exercicios;
    private ArrayList<Serie> series;

    public Cadastro(){
        alunos = new ArrayList<Aluno>();
        exercicios = new ArrayList<Exercicio>();
        series = new ArrayList<Serie>();

        criarExercicios();

        //addAluno("João", "a", "a");
    }

    public ArrayList<Aluno> getAlunos() {
        return alunos;
    }

    public Serie getSerie(int codigoAluno){
        for(Serie s:series){
            if(s.getCodigoAluno() == codigoAluno)
                return s;
        }
        return null;
    }

    public Exercicio getExercicio(int codigoExercicio) {
        for(Exercicio e:exercicios){
            if(e.getCodigo() == codigoExercicio)
                return e;
        }
        return null;
    }

    public Atividade getAtividade(int codigoAluno, int codigoAtividade){
        for(Serie s:series){
            if(s.getCodigoAluno() == codigoAluno){
                for(Atividade a:s.getAtividades()){
                    if(a.getCodigo() == codigoAtividade)
                        return a;
                }
            }
        }
        return null;
    }

    public void addAluno(String nome, String usuario, String senha){
        series.add(new Serie(alunos.size()));
        alunos.add(new Aluno(alunos.size(), usuario, senha, nome));

    }

    public ArrayList<Exercicio> getExercicios() {
        return exercicios;
    }

    private void criarExercicios(){
        Movimento m = new Movimento(1, "Elevação lateral com halter");
        m.setxMin(7);
        m.setyMin(1);
        m.setzMin(2);
        m.setxMax(5);
        m.setyMax(0);
        m.setzMax(-7);
        m.setImagemDrawable(R.drawable.exe1);
        m.setImagemMaiorDrawable(R.drawable.exe1m);
        exercicios.add(m);

        Corrida c = new Corrida(2, "Corrida");
        c.setImagemDrawable(R.drawable.exe2);
        c.setImagemMaiorDrawable(R.drawable.exe2m);
        exercicios.add(c);

        m = new Movimento(3, "Remada vertical com polia");
        m.setxMin(-8);
        m.setyMin(0);
        m.setzMin(3);
        m.setxMax(-8);
        m.setyMax(0);
        m.setzMax(3);
        m.setImagemDrawable(R.drawable.exe3);
        m.setImagemMaiorDrawable(R.drawable.exe3m);
        exercicios.add(m);

        m = new Movimento(4, "Elevação frontal com polia");
        m.setxMin(8);
        m.setyMin(0);
        m.setzMin(3);
        m.setxMax(5);
        m.setyMax(0);
        m.setzMax(-7);
        m.setImagemDrawable(R.drawable.exe4);
        m.setImagemMaiorDrawable(R.drawable.exe4m);
        exercicios.add(m);

        m = new Movimento(5, "Rosca direta punho");
        m.setxMin(6);
        m.setyMin(0);
        m.setzMin(7);
        m.setxMax(-6);
        m.setyMax(2);
        m.setzMax(6);
        m.setImagemDrawable(R.drawable.exe5);
        m.setImagemMaiorDrawable(R.drawable.exe5m);
        exercicios.add(m);

        m = new Movimento(6, "Flexão lateral tronco com halter");
        m.setxMin(8);
        m.setyMin(-1);
        m.setzMin(5);
        m.setxMax(6);
        m.setyMax(-2);
        m.setzMax(6);
        m.setImagemDrawable(R.drawable.exe6);
        m.setImagemMaiorDrawable(R.drawable.exe6m);
        exercicios.add(m);


        /*Serie s = new Serie(0);
        s.addAtividade(0, 5000, 5000, 150);
        s.addAtividade(1, 5000, 5000, 250);
        s.addAtividade(0, 5000, 5000, 350);

        series.add(s);*/
    }
}
