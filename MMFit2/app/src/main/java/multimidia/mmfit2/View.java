package multimidia.mmfit2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.concurrent.TimeUnit;

import static android.app.TimePickerDialog.*;


public class View extends Activity implements OnTimeSetListener {

    private Controle controle;
    private EditText editText1;
    private EditText editText2;
    private EditText editText3;
    private Button button1;
    private Button button2;
    private Button button3;
    private TextView textView1;
    private ImageButton imageButton1;
    private ImageButton imageButton2;
    private ImageButton imageButton3;
    private ImageButton imageButton4;
    private ImageButton imageButton5;
    private ImageButton imageButton6;
    private ViewFlipperAtividades viewFlipperAtividades;
    private int minutosSelecionado, horasSelecionado, distanciaSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(controle == null) {
            controle = new Controle(this);
            login();
        }
    }

    public void login(){
        setContentView(R.layout.login);
        editText1 = (EditText) findViewById(R.id.login_etUsuario);
        editText2 = (EditText) findViewById(R.id.login_etSenha);
        button1 = (Button) findViewById(R.id.login_btLogar);

        button1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.logar(editText1.getText().toString(), editText2.getText().toString());
            }
        });

        button2 = (Button) findViewById(R.id.login_btAcelerometro);
        button2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                Intent myIntent = new Intent(View.this, AccellerationActivity.class);
                startActivityForResult(myIntent, 0);
            }
        });

        //deletar
       // editText1.setText("admin");
       // editText2.setText("admin");
        //deletar
    }

    public void menu(){
        setContentView(R.layout.menu);

        button1 = (Button) findViewById(R.id.menu_btCadastrarAluno);
        button1.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(android.view.View v) {
                cadastroAluno();
            }
        });

        button2 = (Button) findViewById(R.id.menu_btCadastrarTreinamento);
        button2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                cadastroTreinamento();
            }
        });

        button3 = (Button) findViewById(R.id.menu_btSair);
        button3.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.sair();
            }
        });
    }

    public void treino(){
        setContentView(R.layout.treino);

        LinearLayout l = (LinearLayout)findViewById(R.id.treino_llAtividades);
        viewFlipperAtividades = new ViewFlipperAtividades(controle, this);
        l.addView(viewFlipperAtividades);

        button1 = (Button) findViewById(R.id.treino_btIniciar);
        button2 = (Button) findViewById(R.id.treino_btFinalizar);

        button1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.iniciarParar();
            }
        });

        button2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.sair();
            }
        });
    }

    public void cadastroAluno(){
        setContentView(R.layout.cadastro_aluno);

        editText1 = (EditText) findViewById(R.id.cadastro_aluno_etNome);
        editText2 = (EditText) findViewById(R.id.cadastro_aluno_etUsuario);
        editText3 = (EditText) findViewById(R.id.cadastro_aluno_etSenha);
        button1 = (Button) findViewById(R.id.cadastro_aluno_btCadastrar);
        button1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.addAluno(editText1.getText().toString(), editText2.getText().toString(), editText3.getText().toString())){
                    editText1.setText("");
                    editText2.setText("");
                    editText3.setText("");
                    textView1.setText(controle.getListaAlunos());
                }
            }
        });

        button2 = (Button) findViewById(R.id.cadastro_aluno_btRetornar);
        button2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.menu();
            }
        });

        textView1 = (TextView) findViewById(R.id.cadastro_aluno_tvAlunos);
        textView1.setText(controle.getListaAlunos());
    }

    public void cadastroTreinamento(){
        setContentView(R.layout.cadastro_treinamento);

        editText1 = (EditText) findViewById(R.id.cadastro_treinamento_etUsuario);
        textView1 = (TextView) findViewById(R.id.cadastro_treinamento_tvNomeAluno);

        button1 = (Button) findViewById(R.id.cadastro_treinamento_btSelecionarAluno);
        button1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                String nome = controle.selecionarAlunoCadastroTreinamento(editText1.getText().toString());
                textView1.setText(nome);
                if(controle.alunoComAtividade(1)){
                    imageButton1.setBackgroundColor(Color.GREEN);
                }else{
                    imageButton1.setBackgroundColor(Color.GRAY);
                }
                if(controle.alunoComAtividade(2)){
                    imageButton2.setBackgroundColor(Color.GREEN);
                }else{
                    imageButton2.setBackgroundColor(Color.GRAY);
                }
                if(controle.alunoComAtividade(3)){
                    imageButton3.setBackgroundColor(Color.GREEN);
                }else{
                    imageButton3.setBackgroundColor(Color.GRAY);
                }
                if(controle.alunoComAtividade(4)){
                    imageButton4.setBackgroundColor(Color.GREEN);
                }else{
                    imageButton4.setBackgroundColor(Color.GRAY);
                }
                if(controle.alunoComAtividade(5)){
                    imageButton5.setBackgroundColor(Color.GREEN);
                }else{
                    imageButton5.setBackgroundColor(Color.GRAY);
                }
                if(controle.alunoComAtividade(6)){
                    imageButton6.setBackgroundColor(Color.GREEN);
                }else{
                    imageButton6.setBackgroundColor(Color.GRAY);
                }
            }
        });

        button2 = (Button) findViewById(R.id.cadastro_treinamento_btRetornar);
        button2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.menu();
            }
        });

        imageButton1 = (ImageButton) findViewById(R.id.cadastro_treinamento_ib1);
        imageButton2 = (ImageButton) findViewById(R.id.cadastro_treinamento_ib2);
        imageButton3 = (ImageButton) findViewById(R.id.cadastro_treinamento_ib3);
        imageButton4 = (ImageButton) findViewById(R.id.cadastro_treinamento_ib4);
        imageButton5 = (ImageButton) findViewById(R.id.cadastro_treinamento_ib5);
        imageButton6 = (ImageButton) findViewById(R.id.cadastro_treinamento_ib6);
        imageButton1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.alunoComAtividade(1)){
                    controle.remAtividade(1);
                    imageButton1.setBackgroundColor(Color.GRAY);
                }else {
                    horasSelecionado = 0;
                    minutosSelecionado = 0;
                    TimePickerDialog t = new TimePickerDialog(View.this, View.this, 0, 0, true);
                    t.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (horasSelecionado > 0 || minutosSelecionado > 0) {
                                controle.addAtividade(1, (horasSelecionado * 3600000) + (minutosSelecionado * 60000),0);
                                imageButton1.setBackgroundColor(Color.GREEN);
                            }
                        }
                    });
                    t.show();
                }
            }
        });
        imageButton2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.alunoComAtividade(2)){
                    controle.remAtividade(2);
                    imageButton2.setBackgroundColor(Color.GRAY);
                }else {
                    horasSelecionado = 0;
                    minutosSelecionado = 0;
                    TimePickerDialog t = new TimePickerDialog(View.this, View.this, 0, 0, true);
                    t.setTitle("Quanto tempo?");
                    t.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (horasSelecionado > 0 || minutosSelecionado > 0) {
                                distanciaSelecionada = 1;
                                String[] values=new String[1000];
                                for(int i=0;i<values.length;i++){
                                    values[i]=Integer.toString((i+1)*10);
                                }
                                NumberPicker n = new NumberPicker(View.this);
                                n.setMinValue(0);
                                n.setMaxValue(999);
                                n.setDisplayedValues(values);
                                n.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                    @Override
                                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                        distanciaSelecionada = newVal+1;
                                    }
                                });

                                AlertDialog.Builder builder = new AlertDialog.Builder(View.this);
                                builder.setTitle("Quantos metros?");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        controle.addAtividade(2, (horasSelecionado * 3600000) + (minutosSelecionado * 60000), distanciaSelecionada*10);
                                        imageButton2.setBackgroundColor(Color.GREEN);
                                    }
                                });
                                AlertDialog dialog2 = builder.create();
                                dialog2.setView(n);
                                dialog2.show();
                            }
                        }
                    });
                    t.show();




                }
            }
        });
        imageButton3.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.alunoComAtividade(3)){
                    controle.remAtividade(3);
                    imageButton3.setBackgroundColor(Color.GRAY);
                }else {
                    horasSelecionado = 0;
                    minutosSelecionado = 0;
                    TimePickerDialog t = new TimePickerDialog(View.this, View.this, 0, 0, true);
                    t.setTitle("Quanto tempo?");
                    t.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (horasSelecionado > 0 || minutosSelecionado > 0) {
                                controle.addAtividade(3, (horasSelecionado * 3600000) + (minutosSelecionado * 60000),0);
                                imageButton3.setBackgroundColor(Color.GREEN);
                            }
                        }
                    });
                    t.show();
                }
            }
        });
        imageButton4.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.alunoComAtividade(4)){
                    controle.remAtividade(4);
                    imageButton4.setBackgroundColor(Color.GRAY);
                }else {
                    horasSelecionado = 0;
                    minutosSelecionado = 0;
                    TimePickerDialog t = new TimePickerDialog(View.this, View.this, 0, 0, true);
                    t.setTitle("Quanto tempo?");
                    t.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (horasSelecionado > 0 || minutosSelecionado > 0) {
                                controle.addAtividade(4, (horasSelecionado * 3600000) + (minutosSelecionado * 60000),0);
                                imageButton4.setBackgroundColor(Color.GREEN);
                            }
                        }
                    });
                    t.show();
                }
            }
        });
        imageButton5.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.alunoComAtividade(5)){
                    controle.remAtividade(5);
                    imageButton5.setBackgroundColor(Color.GRAY);
                }else {
                    horasSelecionado = 0;
                    minutosSelecionado = 0;
                    TimePickerDialog t = new TimePickerDialog(View.this, View.this, 0, 0, true);
                    t.setTitle("Quanto tempo?");
                    t.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (horasSelecionado > 0 || minutosSelecionado > 0) {
                                controle.addAtividade(5, (horasSelecionado * 3600000) + (minutosSelecionado * 60000),0);
                                imageButton5.setBackgroundColor(Color.GREEN);
                            }
                        }
                    });
                    t.show();
                }
            }
        });
        imageButton6.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(controle.alunoComAtividade(6)){
                    controle.remAtividade(6);
                    imageButton6.setBackgroundColor(Color.GRAY);
                }else {
                    horasSelecionado = 0;
                    minutosSelecionado = 0;
                    TimePickerDialog t = new TimePickerDialog(View.this, View.this, 0, 0, true);
                    t.setTitle("Quanto tempo?");
                    t.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (horasSelecionado > 0 || minutosSelecionado > 0) {
                                controle.addAtividade(6, (horasSelecionado * 3600000) + (minutosSelecionado * 60000),0);
                                imageButton6.setBackgroundColor(Color.GREEN);
                            }
                        }
                    });
                    t.show();
                }
            }
        });
    }

    public void previaTreino(){
        setContentView(R.layout.previa_treino);

        LinearLayout llprincipal = (LinearLayout)findViewById(R.id.previa_treino_llAtividades);
        int aux = 0;

        for(Atividade ativ:controle.getSerieEmExecucao().getAtividades()) {
            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.VERTICAL);
            if(aux % 2 == 0) {
                ll.setBackgroundColor(Color.CYAN);
            }else{
                ll.setBackgroundColor(Color.LTGRAY);
            }
            ll.setLayoutParams(new LinearLayout.LayoutParams(300, LinearLayout.LayoutParams.MATCH_PARENT));

            TextView ordem = new TextView(this);
            ordem.setText(""+(aux+1));
            ordem.setTextSize(50);
            ll.addView(ordem);

            ImageView imagem = new ImageView(this);
            imagem.setImageResource(controle.getExercicio(ativ.getCodigoExercicio()).getImagemDrawable());
            ll.addView(imagem);

            TextView descricao = new TextView(this);
            descricao.setText(controle.getExercicio(ativ.getCodigoExercicio()).getNome());
            descricao.setTextSize(20);
            ll.addView(descricao);

            if(ativ.getQuantidadePrevista() > 0) {
                TextView metros = new TextView(this);
                if((int) (ativ.getQuantidadePrevista() - ativ.getQuantidade()) > 0) {
                    metros.setText("Metros: " + (int) (ativ.getQuantidadePrevista() - ativ.getQuantidade()));
                }else {
                    metros.setText("Metros: 0");
                }
                metros.setTextSize(20);
                ll.addView(metros);
            }

            TextView tempo = new TextView(this);
            //tempo.setText("Tempo: "+(ativ.getTempo()/1000)+ " seg");
            tempo.setText("Tempo:\n"+String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(ativ.getTempo()),
                    TimeUnit.MILLISECONDS.toMinutes(ativ.getTempo()) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ativ.getTempo())),
                    TimeUnit.MILLISECONDS.toSeconds(ativ.getTempo()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((ativ.getTempo())))));
            tempo.setTextSize(20);
            ll.addView(tempo);

            llprincipal.addView(ll);
            aux++;
        }

        button1 = (Button) findViewById(R.id.previa_treino_btTreino);
        button1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                treino();
            }
        });

        button2 = (Button) findViewById(R.id.previa_treino_btSair);
        button2.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                controle.sair();
            }
        });
    }

    public ViewFlipperAtividades getViewFlipperAtividades(){
        return viewFlipperAtividades;
    }

    public Button getButton1(){
        return button1;
    }

    //Não deixa voltar
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        horasSelecionado = hourOfDay;
        minutosSelecionado = minute;
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

}
