package multimidia.mmfit2;



public class Aluno {
    private String nome;
    private int codigo;
    private String usuario;
    private String senha;

    public Aluno(int codigo, String usuario, String senha, String nome){
        this.nome = nome;
        this.codigo = codigo;
        this.usuario = usuario;
        this.senha = senha;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
