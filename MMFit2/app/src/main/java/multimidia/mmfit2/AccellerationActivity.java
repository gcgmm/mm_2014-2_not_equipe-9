package multimidia.mmfit2;


import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class AccellerationActivity extends Activity {

    private TextView result;
    private TextView frase;
    private TextView frase2;

    private SensorManager sensorManager;
    private Sensor sensor;

    private float x, y, z;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelleration);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);

        result = (TextView) findViewById(R.id.result);
        result.setText("No result yet");

        frase = (TextView) findViewById(R.id.frase);

        frase2 = (TextView) findViewById(R.id.frase2);
    }

    private void refreshDisplay() {
        String output = String.format("x is: %f \n y is: %f \n z is: %f", x, y, z);
        if(y < 0) { // O dispositivo esta de cabeça pra baixo
            if(x > 0)
                frase.setText("Virando para ESQUERDA ficando INVERTIDO");
            if(x < 0)
                frase.setText("Virando para DIREITA ficando INVERTIDO");
        } else {
            if(x > 0)
                frase.setText("Virando para ESQUERDA ");
            if(x < 0)
                frase.setText("Virando para DIREITA ");
        }
        if(y < 0){
            frase2.setText("De cabeca para baixo");
        }else{
            frase2.setText("De cabeca para cima");
        }
        if(z < 0){
            frase2.setText(frase2.getText() + "\n" + "inclinado para tras");
        }else{
            frase2.setText(frase2.getText() + "\n" + "inclinado para frente");
        }
        result.setText(output);
    }

    public void executarBeep(){

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(accelerationListener, sensor,
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop() {
        sensorManager.unregisterListener(accelerationListener);
        super.onStop();
    }

    private SensorEventListener accelerationListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
            refreshDisplay();
        }

    };



}