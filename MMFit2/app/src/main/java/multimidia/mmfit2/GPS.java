package multimidia.mmfit2;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GPS {

    private Controle controle;
    private LocationManager manager;
    private LocationListener localListener;
    private double lat1=0, lon1=0, lat2=0, lon2=0;

    public GPS(Controle controle){
        this.controle= controle;
    }

    public void iniciar(){
        localListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lat2 = location.getLatitude();
                lon2 = location.getLongitude();
                if (controle.isExercicioEmExecucao() && lat1 != 0) {
                    if(((int)(controle.getAtividadeEmExecucao().getQuantidadePrevista()
                            - controle.getAtividadeEmExecucao().getQuantidade())) > 0) {
                        float[] results = new float[1];
                        Location.distanceBetween(lat1, lon1, lat2, lon2, results);
                        controle.getAtividadeEmExecucao().setQuantidade(controle.getAtividadeEmExecucao().getQuantidade()
                                + (double) results[0]);
                        controle.getView().getViewFlipperAtividades().setQuantidade("Metros: "
                                + (int) (controle.getAtividadeEmExecucao().getQuantidadePrevista()
                                            - controle.getAtividadeEmExecucao().getQuantidade()));
                    }else{
                        controle.getView().getViewFlipperAtividades().setQuantidade("Terminou");
                    }
                }
                lat1 = lat2;
                lon1 = lon2;
            }
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
            @Override
            public void onProviderEnabled(String provider) {

            }
            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        manager = (LocationManager) controle.getView().getSystemService(Context.LOCATION_SERVICE);
        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, localListener);
    }

    public void parar(){
        if(manager != null)
        manager.removeUpdates(localListener);
    }
}
