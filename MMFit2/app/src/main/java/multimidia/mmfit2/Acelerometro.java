package multimidia.mmfit2;


import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


public class Acelerometro {

    private Controle controle;
    private int margemPrecisao = 3;
    private SensorManager sensorManager;
    private Sensor sensor;
    private float x, y, z;

    public Acelerometro(Controle controle) {
        this.controle = controle;
    }

    public void iniciar(){
        sensorManager = (SensorManager) controle.getView().getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        sensorManager.registerListener(accelerationListener, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void parar(){
        if(sensorManager != null)
        sensorManager.unregisterListener(accelerationListener);
    }

    private void refreshDisplay() {

        if (isIntervalo()) {
            controle.getView().getWindow().getDecorView().setBackgroundColor(Color.GREEN);
            controle.getView().getViewFlipperAtividades().setExtra("CORRETO");
        } else {
            controle.getView().getWindow().getDecorView().setBackgroundColor(Color.RED);
            controle.getView().getViewFlipperAtividades().setExtra("INCORRETO");
        }

    }
    private boolean isIntervalo(){
        float f1 = ((Movimento) controle.getExercicio(controle.getAtividadeEmExecucao().getCodigoExercicio())).getxMin();
        float f2 = ((Movimento) controle.getExercicio(controle.getAtividadeEmExecucao().getCodigoExercicio())).getxMax();
        boolean isValidouX = false;
        boolean isValidouY = false;
        boolean isValidouZ = false;
        /*return (x > (f1-margemPrecisao) && x < (f1+margemPrecisao))
                || (x > (f2-margemPrecisao) && x < (f2+margemPrecisao));*/

        isValidouX = (x > (f1-margemPrecisao) && x < (f1+margemPrecisao))
                || (x > (f2-margemPrecisao) && x < (f2+margemPrecisao));

        f1 = ((Movimento) controle.getExercicio(controle.getAtividadeEmExecucao().getCodigoExercicio())).getyMin();
        f2 = ((Movimento) controle.getExercicio(controle.getAtividadeEmExecucao().getCodigoExercicio())).getyMax();
        isValidouY = (y > (f1-margemPrecisao) && y < (f1+margemPrecisao))
                || (y > (f2-margemPrecisao) && y < (f2+margemPrecisao));

        f1 = ((Movimento) controle.getExercicio(controle.getAtividadeEmExecucao().getCodigoExercicio())).getzMin();
        f2 = ((Movimento) controle.getExercicio(controle.getAtividadeEmExecucao().getCodigoExercicio())).getzMax();
        isValidouZ = (z > (f1-margemPrecisao) && z < (f1+margemPrecisao))
                || (z > (f2-margemPrecisao) && z < (f2+margemPrecisao));

        return isValidouX && isValidouY && isValidouZ;
    }

    private SensorEventListener accelerationListener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(controle.isExercicioEmExecucao()) {
                x = event.values[0];
                y = event.values[1];
                z = event.values[2];
                refreshDisplay();
            }
        }
    };
}
