package multimidia.mmfit2;


import android.graphics.Color;
import android.os.CountDownTimer;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Controle {

    private View view;
    private Cadastro cadastro;
    private int codigoLogado = -1;
    private int codigoAlunoCadastroTreinamento = -1;
    private int codigoAtividadeSelecionada = -1;
    private CountDownTimer contador;
    private GPS gps;
    private Acelerometro acelerometro;

    public Controle(View view){

        this.view = view;
        cadastro = new Cadastro();
        gps = new GPS(this);
        acelerometro = new Acelerometro(this);
    }

    public View getView() {
        return view;
    }

    public void logar(String usuario, String senha) {
        if(usuario.equals("admin") && senha.equals("admin")){
            view.menu();
        }else{
            ArrayList<Aluno> alunos = cadastro.getAlunos();
            for(Aluno a:alunos){
                if(a.getUsuario().equals(usuario) && a.getSenha().equals(senha)) {
                    if(cadastro.getSerie(a.getCodigo()).getAtividades().size() > 0) {
                        codigoLogado = a.getCodigo();
                        view.previaTreino();
                    }else{
                        Toast.makeText(view, "Usuário sem treinamento cadastrado", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public Serie getSerieEmExecucao() {
        return cadastro.getSerie(codigoLogado);
    }



    public Exercicio getExercicio(int codigoExercicio){
        return cadastro.getExercicio(codigoExercicio);
    }

    public Atividade getAtividadeEmExecucao(){
        if(codigoAtividadeSelecionada < 0){
            codigoAtividadeSelecionada = getSerieEmExecucao().getAtividades().get(0).getCodigo();
        }
        return cadastro.getAtividade(codigoLogado, codigoAtividadeSelecionada);
    }



    public void iniciarParar() {
        if(getAtividadeEmExecucao().getTempo() >= 1000 && contador == null){
            contador = new CountDownTimer(getAtividadeEmExecucao().getTempo(), 1000) {

                public void onTick(long millisUntilFinished) {
                    view.getViewFlipperAtividades().setTempo(String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((millisUntilFinished)))));
                    getAtividadeEmExecucao().setTempo(millisUntilFinished);
                }

                public void onFinish() {
                    getAtividadeEmExecucao().setTempo(0);
                    view.getViewFlipperAtividades().setTempo("Esgotado");
                    if(getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Movimento.class)){
                        acelerometro.parar();
                        view.getViewFlipperAtividades().setExtra("");
                    }else if(getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Corrida.class)){
                        gps.parar();
                    }
                    view.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
                }
            }.start();
            view.getButton1().setText("Parar exercício");
            if(getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Corrida.class)){
                gps.iniciar();
            }else if(getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Movimento.class)){
                acelerometro.iniciar();
            }
        }else if(contador != null){
            contador.cancel();
            contador = null;
            view.getButton1().setText("Iniciar exercício");
            view.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            if(getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Movimento.class)){
                acelerometro.parar();
                view.getViewFlipperAtividades().setExtra("");
            }else if(getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Corrida.class)){
                gps.parar();
            }
        }
    }

    public void mudarAtividade(int posicao) {

        if (contador != null) {
            contador.cancel();
            contador = null;
            view.getButton1().setText("Iniciar exercício");
        }
        if (getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Movimento.class)) {
            acelerometro.parar();
            view.getViewFlipperAtividades().setExtra("");
        } else if (getExercicio(getAtividadeEmExecucao().getCodigoExercicio()).getClass().equals(Corrida.class)) {
            gps.parar();
        }
        view.getWindow().getDecorView().setBackgroundColor(Color.WHITE);

        codigoAtividadeSelecionada = getSerieEmExecucao().getAtividades().get(posicao).getCodigo();
    }

    public boolean isExercicioEmExecucao(){
        return contador != null;
    }

    public String getListaAlunos(){
        String s = "";
        for(Aluno a:cadastro.getAlunos()){
            s+= a.getUsuario() + " - " + a.getNome() + "\n";
        }
        return s;
    }

    public boolean addAluno(String nome, String usuario, String senha){
        if(nome.length() > 0 && usuario.length() > 0 && senha.length() > 0) {
            cadastro.addAluno(nome, usuario, senha);
            return true;
        }
        return false;
    }

    public void addAtividade(int codigoExercicio, long tempo, int distancia) {
        if(codigoAlunoCadastroTreinamento >= 0) {
            for (Atividade a : cadastro.getSerie(codigoAlunoCadastroTreinamento).getAtividades()) {
                if (a.getCodigoExercicio() == codigoExercicio) {
                    cadastro.getSerie(codigoAlunoCadastroTreinamento).getAtividades().remove(a);
                    break;
                }
            }
            cadastro.getSerie(codigoAlunoCadastroTreinamento).addAtividade(codigoExercicio, tempo, tempo, distancia);
        }
    }

    public void remAtividade(int codigoExercicio) {
        if(codigoAlunoCadastroTreinamento >= 0) {
            for (Atividade a : cadastro.getSerie(codigoAlunoCadastroTreinamento).getAtividades()) {
                if (a.getCodigoExercicio() == codigoExercicio) {
                    cadastro.getSerie(codigoAlunoCadastroTreinamento).getAtividades().remove(a);
                    break;
                }
            }
        }
    }

    public boolean alunoComAtividade(int codigoExercicio) {
        if(codigoAlunoCadastroTreinamento >= 0) {
            for (Atividade a : cadastro.getSerie(codigoAlunoCadastroTreinamento).getAtividades()) {
                if (a.getCodigoExercicio() == codigoExercicio) {
                    return true;
                }
            }
        }
        return false;
    }

    public String selecionarAlunoCadastroTreinamento(String usuario) {
        ArrayList<Aluno> alunos = cadastro.getAlunos();
        for(Aluno a:alunos){
            if(a.getUsuario().equals(usuario)) {
                codigoAlunoCadastroTreinamento = a.getCodigo();
                return a.getNome();
            }
        }
        codigoAlunoCadastroTreinamento = -1;
        return "";
    }

    public void sair(){
        if(contador != null) contador.cancel();
        contador = null;
        gps.parar();
        acelerometro.parar();
        codigoLogado = -1;
        codigoAlunoCadastroTreinamento = -1;
        codigoAtividadeSelecionada = -1;
        view.getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        view.login();
    }

    public void menu(){
        codigoLogado = -1;
        codigoAlunoCadastroTreinamento = -1;
        codigoAtividadeSelecionada = -1;
        view.menu();
    }

}
